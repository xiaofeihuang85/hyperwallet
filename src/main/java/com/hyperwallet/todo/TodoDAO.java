package com.hyperwallet.todo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

/**
 * Business layer/Controller calls DAO to manipulate TodoItem.
 * 
 * Only data manipulation involved; no business logic in this layer.
 * 
 * @author xiaof
 *
 */
@Component
public class TodoDAO {
    /**
     * Dummy database that holds todoItems in current session.
     */
    private Map<String, TodoItem> todoItems = new HashMap<>();

    /**
     * If given TodoItem exists.
     * 
     * @param todoItemTitle
     *            the title of the TodoItem
     * @return true if the TodoItem exists
     */
    public boolean contains(String todoItemTitle) {
        return this.todoItems.containsKey(todoItemTitle);
    }

    /**
     * Get Todoitem.
     * 
     * @param todoItemTitle
     *            retrieve to do item by its title.
     * @return the requested TodoItem
     */
    public TodoItem get(String todoItemTitle) {
        return this.todoItems.get(todoItemTitle);
    }

    /**
     * Save Todoitems.
     * 
     * @param todoItems
     *            the list of to do items which you want to save
     */
    public void createTodoItems(List<TodoItem> todoItems) {
        for (TodoItem todoItem : todoItems) {
            this.todoItems.put(todoItem.getTitle(), todoItem);
        }
    }

    /**
     * Save TotoItem.
     * 
     * @param todoItem
     *            to be saved TodoItem object
     */
    public void createTodoItem(TodoItem todoItem) {
        this.todoItems.put(todoItem.getTitle(), todoItem);
    }

    /**
     * 
     * @return all the TodoItems
     */
    public List<TodoItem> getTodoItems() {
        return new ArrayList<>(this.todoItems.values());
    }

    /**
     * Update TodoItem
     * 
     * @param todoItemTitle
     *            the title of the to be updated TodoItem object
     * @param todoItem
     *            to be updated TodoItem object
     */
    public void updateTodoItem(String todoItemTitle, TodoItem todoItem) {
        todoItem.setTitle(todoItemTitle);
        this.todoItems.put(todoItemTitle, todoItem);
    }

    /**
     * Delete TodoItem
     * 
     * @param todoItemTitle
     *            used as Id to delete an TodoItem object
     */
    public void deleteTodoItem(String todoItemTitle) {
        this.todoItems.remove(todoItemTitle);
    }

    /**
     * Delete all the TodoItems
     */
    public void deleteTodoItems() {
        this.todoItems = new HashMap<>();
    }
}
