package com.hyperwallet.todo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller that receives all the REST service requests from clients
 * 
 * @author xiaof
 *
 */
@RestController
@RequestMapping("/todoItems")
public class TodoController {

    @Autowired
    private TodoDAO todoDAO;

    /**
     * Get TodoItem.
     * 
     * @param todoItemTitle
     *            retrieve TodoItem by its title
     * @return the requested TodoItem
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{todoItemTitle}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTodoItem(@PathVariable String todoItemTitle) {
        if (todoDAO.contains(todoItemTitle)) {
            return new ResponseEntity<TodoItem>(todoDAO.get(todoItemTitle), HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Could not find a todoItem with title: " + todoItemTitle);
        }
    }

    /**
     * Get all TodoItems.
     * 
     * @return a list of current TodoItems
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTodoItems() {
        return new ResponseEntity<List<TodoItem>>(todoDAO.getTodoItems(), HttpStatus.OK);
    }

    /**
     * Save TodoItems list.
     * 
     * @param todoItems
     *            the list of TodoItems which you want to save
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createTodoItems(@RequestBody List<TodoItem> todoItems) {
        for (TodoItem todoItem : todoItems) {
            if (StringUtils.isEmpty(todoItem.getTitle().trim())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("One of the TodoItems does not have title!");
            }

            if (todoDAO.contains(todoItem.getTitle())) {
                return ResponseEntity.status(HttpStatus.CONFLICT)
                        .body("The TodoItem with title " + todoItem.getTitle() + " already exists!");
            }
        }

        todoDAO.createTodoItems(todoItems);
        return ResponseEntity.status(HttpStatus.CREATED).body("created");
    }

    /**
     * Save TodoItem.
     * 
     * @param todoItem
     *            to be saved TodoItem object
     * @return saved TodoItem
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createTodoItem(@RequestBody TodoItem todoItem) {
        if (todoDAO.contains(todoItem.getTitle())) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body("The TodoItem with title " + todoItem.getTitle() + " already exists!");
        } else {
            todoDAO.createTodoItem(todoItem);
            return ResponseEntity.status(HttpStatus.CREATED).body("created");
        }
    }

    /**
     * Update TodoItem
     * 
     * @param todoItemTitle
     *            used as Id to update an TodoItem object
     * @param todoItem
     *            the TodoItem object to be updated
     * @return updated TodoItem or error message
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/{todoItemTitle}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateTodoItem(@PathVariable String todoItemTitle, @RequestBody TodoItem todoItem) {
        if (todoDAO.contains(todoItemTitle)) {
            todoDAO.updateTodoItem(todoItemTitle, todoItem);
            return new ResponseEntity<TodoItem>(todoItem, HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Could not find a todoItem with title: " + todoItemTitle);
        }
    }

    /**
     * Delete TodoItem
     * 
     * @param todoItemTitle
     *            used as Id to delete an TodoItem object
     * @return deleted TodoItem object
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{todoItemTitle}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteTodoItem(@PathVariable String todoItemTitle) {
        if (todoDAO.contains(todoItemTitle)) {
            TodoItem todoItem = todoDAO.get(todoItemTitle);
            todoDAO.deleteTodoItem(todoItemTitle);
            return new ResponseEntity<TodoItem>(todoItem, HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Could not find a todoItem with title: " + todoItemTitle);
        }
    }

    /**
     * Delete all the TodoItems
     */
    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteTodoItems() {
        todoDAO.deleteTodoItems();
        return ResponseEntity.status(HttpStatus.OK).body("All TodoItems has been deleted!");
    }
}
