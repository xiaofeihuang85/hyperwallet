package com.hyperwallet.todo;

/**
 * Model for to do item
 * 
 * @author xiaof
 *
 */
public class TodoItem {
    /**
     * Id
     */
    private String title;
    private String description;
    private boolean completed = false;

    public TodoItem() {
        super();
    }

    public TodoItem(String title, String description, boolean completed) {
        super();
        this.title = title;
        this.description = description;
        this.completed = completed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
