package com.hyperwallet.todo;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TodoApplicationTests {

    @Rule
    public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    private ObjectMapper mapper;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation)).alwaysDo(document("{method-name}/{step}/"))
                .build();

        mapper = new ObjectMapper();
        // Clear TodoItem list before each test
        this.mockMvc.perform(delete("/todoItems")).andExpect(status().isOk());
    }

    @Test
    public void get_todoItem_ok() throws Exception {
        this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(new TodoItem("todoItem0", "get_todoItem_ok_item0", true))))
                .andExpect(status().isCreated());
        this.mockMvc.perform(get("/todoItems/todoItem0"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is("todoItem0")))
                .andExpect(jsonPath("$.description", is("get_todoItem_ok_item0")))
                .andExpect(jsonPath("$.completed", is(true)));
    }

    @Test
    public void get_todoItem_notFound() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/todoItems/notFound"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isNotFound())
                .andReturn();
        assertTrue(result.getResponse().getContentAsString().equals("Could not find a todoItem with title: notFound"));
    }

    @Test
    public void get_todoItems_empty_ok() throws Exception {
        this.mockMvc.perform(get("/todoItems")).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void get_todoItems_ok() throws Exception {
        this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(new TodoItem("todoItem0", "get_todoItems_ok_item0", true))))
                .andExpect(status().isCreated());
        this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(new TodoItem("todoItem1", "get_todoItems_ok_item1", true))))
                .andExpect(status().isCreated());
        this.mockMvc.perform(get("/todoItems")).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].title", is("todoItem1")))
                .andExpect(jsonPath("$[0].description", is("get_todoItems_ok_item1")))
                .andExpect(jsonPath("$[0].completed", is(true))).andExpect(jsonPath("$[1].title", is("todoItem0")))
                .andExpect(jsonPath("$[1].description", is("get_todoItems_ok_item0")))
                .andExpect(jsonPath("$[1].completed", is(true)));
    }

    @Test
    public void create_todoItems_created() throws Exception {
        List<TodoItem> todoItems = Arrays.asList(new TodoItem("todoItem0", "create_todoItems_created_item0", false),
                new TodoItem("todoItem1", "create_todoItems_created_item1", true));
        MvcResult result = this.mockMvc.perform(post("/todoItems/list").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(todoItems))).andExpect(status().isCreated()).andReturn();
        assertTrue(result.getResponse().getContentAsString().equals("created"));
    }

    @Test
    public void create_todoItems_emptyTitle_badRequest() throws Exception {
        List<TodoItem> todoItems = Arrays.asList(
                new TodoItem("todoItem0", "create_todoItems_emptyTitle_badRequest_item0", false),
                new TodoItem("  ", "create_todoItems_emptyTitle_badRequest_item1", true));
        MvcResult result = this.mockMvc.perform(post("/todoItems/list").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(todoItems))).andExpect(status().isBadRequest()).andReturn();
        assertTrue(result.getResponse().getContentAsString().equals("One of the TodoItems does not have title!"));
    }

    @Test
    public void create_todoItems_duplicates_conflict() throws Exception {
        this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(
                                new TodoItem("todoItem0", "create_todoItems_existing_badRequest_item0", false))))
                .andExpect(status().isCreated());

        List<TodoItem> todoItems = Arrays.asList(
                new TodoItem("todoItem0", "create_todoItems_existing_badRequest_item0", false),
                new TodoItem("todoItem1", "create_todoItems_existing_badRequest_item1", true));
        MvcResult result = this.mockMvc.perform(post("/todoItems/list").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(todoItems))).andExpect(status().isConflict()).andReturn();
        assertTrue(
                result.getResponse().getContentAsString().equals("The TodoItem with title todoItem0 already exists!"));
    }

    @Test
    public void create_todoItem_created() throws Exception {
        MvcResult result = this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        mapper.writeValueAsString(new TodoItem("todoItem0", "create_todoItem_created_item0", true))))
                .andExpect(status().isCreated()).andReturn();

        assertTrue(result.getResponse().getContentAsString().equals("created"));
    }

    @Test
    public void create_todoItem_duplicates_conflict() throws Exception {
        this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(
                                new TodoItem("todoItem0", "create_todoItem_duplicates_conflict_item0", true))))
                .andExpect(status().isCreated());
        MvcResult result = this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(
                                new TodoItem("todoItem0", "create_todoItem_duplicates_conflict_item0", true))))
                .andExpect(status().isConflict()).andReturn();

        assertTrue(
                result.getResponse().getContentAsString().equals("The TodoItem with title todoItem0 already exists!"));
    }

    @Test
    public void update_todoItem_updated() throws Exception {
        this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        mapper.writeValueAsString(new TodoItem("todoItem0", "update_todoItem_updated_item0", true))))
                .andExpect(status().isCreated());

        this.mockMvc
                .perform(put("/todoItems/todoItem0").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(
                                new TodoItem("todoItem0", "update_todoItem_updated_item0_updated", false))))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is("todoItem0")))
                .andExpect(jsonPath("$.description", is("update_todoItem_updated_item0_updated")))
                .andExpect(jsonPath("$.completed", is(false)));
    }

    @Test
    public void update_todoItem_notFound() throws Exception {
        this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        mapper.writeValueAsString(new TodoItem("todoItem0", "update_todoItem_updated_item0", true))))
                .andExpect(status().isCreated());

        MvcResult result = this.mockMvc
                .perform(put("/todoItems/todoItem1").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(
                                new TodoItem("todoItem1", "update_todoItem_updated_item1_updated", false))))
                .andExpect(status().isNotFound()).andReturn();

        assertTrue(result.getResponse().getContentAsString().equals("Could not find a todoItem with title: todoItem1"));
    }

    @Test
    public void delete_todoItem_ok() throws Exception {
        this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        mapper.writeValueAsString(new TodoItem("todoItem0", "delete_todoItem_ok_item0", true))))
                .andExpect(status().isCreated());

        this.mockMvc.perform(delete("/todoItems/todoItem0"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is("todoItem0")))
                .andExpect(jsonPath("$.description", is("delete_todoItem_ok_item0")))
                .andExpect(jsonPath("$.completed", is(true)));
    }

    @Test
    public void delete_todoItem_notFound() throws Exception {
        this.mockMvc
                .perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        mapper.writeValueAsString(new TodoItem("todoItem0", "delete_todoItem_ok_item0", true))))
                .andExpect(status().isCreated());

        MvcResult result = this.mockMvc.perform(delete("/todoItems/todoItem1")).andExpect(status().isNotFound())
                .andReturn();
        assertTrue(result.getResponse().getContentAsString().equals("Could not find a todoItem with title: todoItem1"));
    }

    @Test
    public void delete_todoItems_ok() throws Exception {
        List<TodoItem> todoItems = Arrays.asList(new TodoItem("todoItem0", "delete_todoItems_ok_item0", false),
                new TodoItem("todoItem1", "delete_todoItems_ok_item1", true));
        this.mockMvc.perform(post("/todoItems/list").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(todoItems))).andExpect(status().isCreated());

        this.mockMvc.perform(get("/todoItems/todoItem0")).andExpect(status().isOk());
        this.mockMvc.perform(get("/todoItems/todoItem1")).andExpect(status().isOk());
        MvcResult result = this.mockMvc.perform(delete("/todoItems")).andExpect(status().isOk()).andReturn();
        assertTrue(result.getResponse().getContentAsString().equals("All TodoItems has been deleted!"));
        this.mockMvc.perform(get("/todoItems/todoItem0")).andExpect(status().isNotFound());
        this.mockMvc.perform(get("/todoItems/todoItem1")).andExpect(status().isNotFound());
    }
}
